//
//  FeedViewModel.swift
//  threads
//
//  Created by Alexey Khomych on 28.10.2023.
//

import Foundation
import Combine

class FeedViewModel: ObservableObject {
    
    @Published var threads = [Thread]()
    
    init() {
        Task {
            try await fetchThreads()
        }
    }
    
    @MainActor
    func fetchThreads() async throws {
        threads = try await ThreadService.fetchThreads()
        try await fetchUserDataForThreads()
    }
}

// MARK: - Private

private extension FeedViewModel {
    
    @MainActor
    func fetchUserDataForThreads() async throws {
        for (index, thread) in threads.enumerated() {
            let ownerUid = thread.ownerUid
            let user = try await UserService.fetchUser(withUid: ownerUid)
            threads[index].user = user
        }
    }
}
