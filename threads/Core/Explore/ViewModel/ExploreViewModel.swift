//
//  ExploreViewModel.swift
//  threads
//
//  Created by Alexey Khomych on 26.10.2023.
//

import Foundation

class ExploreViewModel: ObservableObject {
    
    @Published var users = [User]()
    
    init() {
        Task {
            try await fetchUsers()
        }
    }
    
    
}

// MARK: - Private

private extension ExploreViewModel {
    
    @MainActor
    func fetchUsers() async throws {
        users = try await UserService.fetchUsers()
    }
}
