//
//  ExploreCell.swift
//  threads
//
//  Created by Alexey Khomych on 16.10.2023.
//

import SwiftUI

struct ExploreCell: View {
    
    let user: User
    
    var body: some View {
        VStack {
            HStack(alignment: .top, spacing: 10) {
                CircularProfileImageView(imageUrl: user.profileImageUrl, size: .small)
                    
                VStack(alignment: .leading) {
                    Text(user.username)
                        .fontWeight(.bold)
                    Text(user.fullname)
                        .fontWeight(.regular)
                }
                
                Spacer()
                
                Button {
                    
                } label: {
                    Text("Follow")
                        .fontWeight(.bold)
                        .padding([.top, .bottom], 10)
                        .padding([.trailing, .leading], 30)
                }
                .foregroundStyle(.black)
                .overlay {
                    RoundedRectangle(cornerRadius: 10)
                        .stroke(Color(.systemGray4), lineWidth: 1)
                }
            }
            .padding(.horizontal)
        }
        .padding(.vertical, 4)
    }
}

#Preview {
    ExploreCell(user: DeveloperPreview.user)
}
