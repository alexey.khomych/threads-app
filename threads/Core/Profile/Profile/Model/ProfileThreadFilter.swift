//
//  ProfileThreadFilter.swift
//  threads
//
//  Created by Alexey Khomych on 24.10.2023.
//

import Foundation

enum ProfileThreadFilter: Int, CaseIterable, Identifiable {
    case threads
    case replies
    
    var title: String {
        switch self {
            case .threads: "Threads"
            case .replies: "Replies"
        }
    }
    
    var id: Int { self.rawValue }
}
