//
//  ProfileHeaderView.swift
//  threads
//
//  Created by Alexey Khomych on 27.10.2023.
//

import SwiftUI

struct ProfileHeaderView: View {
    
    var user: User?
    
    init(user: User? = nil) {
        self.user = user
    }
    
    var body: some View {
        HStack(alignment: .top) {
            VStack(alignment: .leading, spacing: 12) {
                VStack(alignment: .leading, spacing: 4) {
                    Text(user?.fullname ?? "")
                        .font(.title2)
                        .fontWeight(.semibold)
                    
                    Text(user?.username ?? "")
                        .font(.subheadline)
                }
                
                if let bio = user?.bio {
                    Text(bio)
                        .font(.footnote)
                }
                
                Text("2 followers")
                    .font(.caption2)
                    .foregroundStyle(.gray)
            }
            
            Spacer()
            
            CircularProfileImageView(imageUrl: user?.profileImageUrl, size: .small)
        }
    }
}

#Preview {
    ProfileHeaderView(user: DeveloperPreview.user)
}
