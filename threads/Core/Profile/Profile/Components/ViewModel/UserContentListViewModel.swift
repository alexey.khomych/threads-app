//
//  UserContentListViewModel.swift
//  threads
//
//  Created by Alexey Khomych on 30.10.2023.
//

import Foundation

class UserContentListViewModel: ObservableObject {
    
    @Published var threads = [Thread]()
    
    let user: User
    
    init(user: User) {
        self.user = user
        Task {
            try await fetchUserThreads()
        }
    }
    
    @MainActor
    func fetchUserThreads() async throws {
        threads = try await ThreadService.fetchUserThreads(uid: user.id)
        for (index, _) in threads.enumerated() {
            threads[index].user = user
        }
    }
}
