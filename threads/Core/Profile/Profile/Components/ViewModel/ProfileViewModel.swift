//
//  ProfileViewModel.swift
//  threads
//
//  Created by Alexey Khomych on 28.10.2023.
//

import Foundation

class ProfileViewModel: ObservableObject {
    
    @Published var items = [Thread]()
    
    @MainActor
    func loadThreadList(forUser user: User) async throws {
        items = try await ThreadService.fetchThreads()
    }
}
