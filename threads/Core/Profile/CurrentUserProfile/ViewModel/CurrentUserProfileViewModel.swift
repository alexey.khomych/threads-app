//
//  CurrentUserProfileViewModel.swift
//  threads
//
//  Created by Alexey Khomych on 26.10.2023.
//

import Combine
import SwiftUI

class CurrentUserProfileViewModel: ObservableObject {
    
    @Published var currentUser: User?
    @Published var items = [Thread]()
    
    private var cancellables = Set<AnyCancellable>()
    
    init() {
        setupSubscribers()
    }
    
    func saveChanges() {
        
    }
    
    @MainActor
    func fetchThreads() async throws {
        guard let currentUser else { return }
        items = try await ThreadService.fetchUserThreads(uid: currentUser.id)
    }
}

// MARK: - Private

private extension CurrentUserProfileViewModel {
    
    func setupSubscribers() {
        UserService.shared.$currentUser
            .receive(on: DispatchQueue.main)
            .sink { [weak self] user in
            DispatchQueue.main.async {
                self?.currentUser = user
            }
        }.store(in: &cancellables)
    }
    
    @MainActor
    func fetchUserDataForThreads() async throws {

    }
}
