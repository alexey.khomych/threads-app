//
//  EditProfileViewModel.swift
//  threads
//
//  Created by Alexey Khomych on 27.10.2023.
//

import SwiftUI
import Combine
import PhotosUI
import FirebaseFirestoreSwift

class EditProfileViewModel: ObservableObject {
    
    @Published var bio = ""
    @Published var link = ""
    @Published var isPrivateProfile = false
    
    @Published var selectedItem: PhotosPickerItem? {
        didSet {
            Task {
                await loadImage()
            }
        }
    }
    @Published var profileImage: Image?
    
    private var uiImage: UIImage?
    
    func saveChanges() async throws {
        try await updateProfileImage()
    }
}

// MARK: - Private

private extension EditProfileViewModel {
    
    @MainActor
    func loadImage() async {
        guard let selectedItem else { return }
        guard let data = try? await selectedItem.loadTransferable(type: Data.self) else { return }
        guard let uiImage = UIImage(data: data) else { return }
        
        self.uiImage = uiImage
        profileImage = Image(uiImage: uiImage)
    }
    
    @MainActor
    func updateProfileImage() async throws {
        guard let uiImage else { return }
        guard let imageURL = try? await ImageUploader.uploadImage(uiImage) else { return }
        try await UserService.shared.updateUserProfileImage(withImageUrl: imageURL)
    }
}
