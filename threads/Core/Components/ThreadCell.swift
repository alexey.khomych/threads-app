//
//  ThreadCell.swift
//  threads
//
//  Created by Alexey Khomych on 16.10.2023.
//

import SwiftUI

struct ThreadCell: View {
    
    let thread: Thread
    
    var body: some View {
        VStack {
            HStack(alignment: .top, spacing: 10) {
                CircularProfileImageView(imageUrl: thread.user?.profileImageUrl, size: .small)
                    
                VStack(alignment: .leading) {
                    HStack(spacing: 10) {
                        Text(thread.user?.fullname ?? "")
                            .fontWeight(.bold)
                        
                        Spacer()
                        
                        Text(thread.timestamp.timestampString())
                            .foregroundStyle(.gray)
                        
                        Button {
                            
                        } label: {
                            Text("...")
                                .fontWeight(.bold)
                        }
                        .foregroundStyle(.black)
                    }
                    
                    Text(thread.caption)
                        .fontWeight(.regular)
                    
                    HStack(spacing: 20) {
                        
                        Button {
                            
                        } label: {
                            Image(systemName: "heart")
                        }
                        .modifier(PostButtonsModifier())
                        
                        Button {
                            
                        } label: {
                            Image(systemName: "bubble.right")
                        }
                        .modifier(PostButtonsModifier())
                        
                        Button {
                            
                        } label: {
                            Image(systemName: "arrow.rectanglepath")
                        }
                        .modifier(PostButtonsModifier())
                        
                        Button {
                            
                        } label: {
                            Image(systemName: "paperplane")
                        }
                        .modifier(PostButtonsModifier())
                    }
                    .padding(.top, 10)
                    
                    Divider()
                }
            }
        }
        .padding()
    }
}

#Preview {
    ThreadCell(thread: DeveloperPreview.thread)
}
