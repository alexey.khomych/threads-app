//
//  CircularProfileImageView.swift
//  threads
//
//  Created by Alexey Khomych on 16.10.2023.
//

import SwiftUI
import Kingfisher

enum ProfileImageSize {
    case xxSmall
    case xSmall
    case small
    case medium
    case large
    case xLarge
    
    var dimension: CGFloat {
        switch self {
            case .xxSmall:
                28
            case .xSmall:
                32
            case .small:
                40
            case .medium:
                48
            case .large:
                64
            case .xLarge:
                80
        }
    }
}

struct CircularProfileImageView: View {
    
    var imageUrl: String?
    let size: ProfileImageSize
    
    var body: some View {
        if let imageUrl {
            KFImage(URL(string: imageUrl))
                .resizable()
                .scaledToFill()
                .frame(width: size.dimension, height: size.dimension)
                .clipShape(Circle())
        } else {
            Image(systemName: "person.circle.fill")
                .resizable()
                .frame(width: size.dimension, height: size.dimension)
                .foregroundStyle(Color(.systemGray4))
        }
    }
}

#Preview {
    CircularProfileImageView(imageUrl: nil, size: .medium)
}
