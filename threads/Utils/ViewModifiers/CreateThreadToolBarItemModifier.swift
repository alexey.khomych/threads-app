//
//  CreateThreadToolBarItemModifier.swift
//  threads
//
//  Created by Alexey Khomych on 24.10.2023.
//

import SwiftUI

struct CreateThreadToolBarItemModifier: ViewModifier {
    func body(content: Content) -> some View {
        content
            .font(.subheadline)
            .foregroundStyle(.black)
    }
}
