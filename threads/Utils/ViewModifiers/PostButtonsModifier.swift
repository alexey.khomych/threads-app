//
//  PostButtonsModifier.swift
//  threads
//
//  Created by Alexey Khomych on 16.10.2023.
//

import SwiftUI

struct PostButtonsModifier: ViewModifier {
    func body(content: Content) -> some View {
        content
            .frame(width: 20, height: 20)
            .foregroundStyle(.black)
    }
}
