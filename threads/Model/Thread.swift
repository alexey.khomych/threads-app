//
//  Thread.swift
//  threads
//
//  Created by Alexey Khomych on 28.10.2023.
//

import Foundation
import Firebase
import FirebaseFirestoreSwift

struct Thread: Identifiable, Codable {
    @DocumentID var threadId: String?
    let ownerUid: String
    let caption: String
    let timestamp: Timestamp
    var likes: Int
    var user: User?
    
    var id: String {
        threadId ?? NSUUID().uuidString
    }
}
