//
//  PreviewProvider.swift
//  threads
//
//  Created by Alexey Khomych on 26.10.2023.
//

import SwiftUI
import Firebase

class DeveloperPreview {
    
    static let user = User(id: UUID().uuidString, fullname: "Alex Furious", email: "alexeyfurious@gmail.com", username: "alexeyfurious")
    static let thread = Thread(ownerUid: UUID().uuidString, caption: "some caption", timestamp: Timestamp(), likes: 0)
}
